﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class InputManager : MonoBehaviour
{

    private void Awake()
    {
        Instance = this;
    }

    public static InputManager Instance;

    public class TouchData
    {
        public Touch touch;
        public Vector3 beginPosition;
        public TouchData(Touch t)
        {
            touch = t;
            beginPosition = t.position;
        }
    }

    float prevDistace = 0;
    private Dictionary<int, TouchData> touchDic = new Dictionary<int, TouchData>();

    private void Update()
    {
#if UNITY_EDITOR


#else
        foreach (Touch touch in Input.touches)
        {
            if (!touchDic.ContainsKey(touch.fingerId))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                GameObject hitObj = null;
                if (Physics.Raycast(ray, out hit)) 
                {
                    hitObj = hit.collider.gameObject;
                }
                touchDic.Add(touch.fingerId, new TouchData(touch));
            }
            else
            {
                touchDic[touch.fingerId].touch = touch;
            }
        }

#endif
    }
    private void LateUpdate()
    {
        RemoveTouchData();
    }

    public Vector3? GetPosition(int id)
    {
#if UNITY_EDITOR
        return (Input.GetMouseButton(id)) ? (Vector3?)Input.mousePosition : null;
#else
        return  touchDic.ContainsKey(id) ? (Vector3?)touchDic[id].touch.position : null;
#endif
    }

    public Vector3? GetBeginPosition(int id)
    {
        return touchDic.ContainsKey(id) ? (Vector3?)touchDic[id].beginPosition : null;

    }

    public float GetDistance(int firstId, int secondId)
    {
        float dis = -1f;
        if (touchDic.ContainsKey(firstId) && touchDic.ContainsKey(secondId))
        {
            dis = Vector2.Distance(touchDic[firstId].touch.position, touchDic[secondId].touch.position);
        }
        return dis;
    }

    public float GetDeltaDistance(int firstId, int secondId)
    {
        float distace = GetDistance(firstId, secondId);
        if (distace == -1f)
        {
            return 0;
        }
        if (prevDistace == 0)
        {
            prevDistace = distace;
        }

        float deltaDistace = distace - prevDistace;
        prevDistace = distace;

        return deltaDistace;
        

    }

    public Vector3? GetDeltaPosition(int id)
    {

#if UNITY_EDITOR
        if (Input.GetMouseButton(id))
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");
            Vector3? delta = new Vector3(mouseX, mouseY, 0);
            return delta;
        }
        else
        {
            return null;
        }
#else
        return touchDic.ContainsKey(id) ? (Vector3?)touchDic[id].touch.deltaPosition : null;
#endif
    }


    private void RemoveTouchData()
    {


        var removeTarget = touchDic.Where(w => w.Value.touch.phase == TouchPhase.Ended || w.Value.touch.phase == TouchPhase.Canceled).ToArray();

        foreach(var item in removeTarget)
        {
            touchDic.Remove(item.Key);
        }
        if (touchDic.Count <= 1)
        {
            prevDistace = 0;
        }
    }


    public int GetInputCount()
    {
        return Input.touchCount;
    }

 



}
