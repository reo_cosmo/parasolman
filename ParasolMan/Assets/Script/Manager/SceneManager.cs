﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour
{
    public enum SceneState
    {
        Title,
        StageSelect,
        Main,
        Result
    }

    [SerializeField]
    GameObject canvasObject;
    [SerializeField]
    GameObject sceneObject;

    [SerializeField]
    GameObject mainObject;

    [SerializeField]
    GameManager gameManager;


    GameObject[] canvasChildObject;
    GameObject[] sceneChildObject;

    GameObject currentCanvas;
    GameObject currentObject;


    //プレイカウント アプリ評価表示をするまでのカウント
    int playCount = 0;
    [Header("アプリ評価表示までに何度プレイさせるか")]
    [SerializeField]
    int rateShowCount;

    //広告表示カウント
    int adsCount = 0;
    
    [Header("広告表示までに何度プレイさせるか")]
    [SerializeField]
    int adsShowCount;

    //リトライフラグ
    bool retried;
    [SerializeField]
    Button retryButton;

    [SerializeField]
    Text resultText;


    private void Awake()
    {
        adsCount = 0;
    }


    public SceneState currentSceneState
    {
        private set;
        get;
    }

    public static SceneManager instance;


    private void Start()
    {
        instance = this;
        currentSceneState = SceneState.Title;

        int count = canvasObject.transform.childCount;
        canvasChildObject = new GameObject[count];
        for (int i = 0; i < count; i++)
        {
            canvasChildObject[i] = canvasObject.transform.GetChild(i).gameObject;
            bool isActive = canvasChildObject[i].name.Contains( currentSceneState.ToString() );
            canvasChildObject[i].SetActive(isActive);
            if (isActive)
            {
                currentCanvas = canvasChildObject[i];
            }
        }

        count = sceneObject.transform.childCount;
        sceneChildObject = new GameObject[count];
        for (int i = 0; i < count; i++)
        {
            sceneChildObject[i] = sceneObject.transform.GetChild(i).gameObject;
            bool isActive = sceneChildObject[i].name.Contains(currentSceneState.ToString());
            sceneChildObject[i].SetActive(isActive);
            if (isActive)
            {
                currentObject = sceneChildObject[i];
            }
        }



    }


    [EnumAction(typeof(SceneState))]
    public void ChangeScene( int nextSceneNum )
    {
        //Debug.Log(nextSceneNum);
        SceneState nextScene = (SceneState)nextSceneNum;
        currentObject?.SetActive(false);
        currentObject = sceneChildObject.Where(w => w.name.Contains(nextScene.ToString())).FirstOrDefault();
        currentObject?.SetActive(true);


        currentCanvas?.SetActive(false);
        currentCanvas = canvasChildObject.Where(w => w.name.Contains(nextScene.ToString())).FirstOrDefault();
        currentCanvas?.SetActive(true);
        currentSceneState = nextScene;

        foreach (Transform mo in mainObject.transform)
        {
            Destroy(mo.gameObject);
        }



        if(nextScene == SceneState.Title)
        {
            //タイトルに戻るとリトライフラグをリセットする
            retried = false;
            gameManager.score = 0;
            
        }
        //ゲーム開始
        else if (nextScene == SceneState.Main)
        {
            gameManager.InitGame();
            gameManager.isPlaying = true;
        }
        else if(nextScene == SceneState.Result)
        {
            resultText.text = "Fall\t???m\n\n" + "Coin\t???c\n\n" + "Score\t" + gameManager.score.ToString();



            if(retried == true)
            {
                retryButton.GetComponent<Text>().text = "";
                retryButton.interactable = false;

                /*
             //リトライした後ならリトライボタンをタイトルへ戻るボタンへ変更する
                retryButton.GetComponent<Text>().text = "Touch to Title";
                retryButton.onClick.RemoveAllListeners();
                retryButton.onClick.AddListener(() => SceneManager.instance.ChangeScene((int)SceneManager.SceneState.Title));    
             */
            }
            else
            {
                retryButton.interactable = true;
                retryButton.GetComponent<Text>().text = "Touch to Retry\n(Show Ads)";
                retryButton.onClick.RemoveAllListeners();
                retryButton.onClick.AddListener(() => Retry());
                retried = true;
            }

            //広告判定
            adsCount++;
            if(adsCount > adsShowCount)
            {
                adsCount = 0;
                //広告表示
                Debug.Log("広告表示");
            }

            playCount++;
            if(playCount > rateShowCount)
            {
                playCount = 0;
                //アプリ評価表示
                Debug.Log("アプリ評価表示");
            }
        }
    }


    void Retry()
    {
        //広告を見たのちリトライする

        Debug.Log("リワード広告表示");

        SceneManager.instance.ChangeScene((int)SceneManager.SceneState.Main);

    }

}
