﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class StageScriptableObject : ScriptableObject
{
    [SerializeField]
    public GameObject[] gameObjects;

    [SerializeField]
    public GameObject[] bird;
}
