﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class StageSelectScrollView : MonoBehaviour
{
    [SerializeField]
    StageScriptableObject stageData;

    [SerializeField]
    GameObject buttonBase;

    [SerializeField]
    ScrollRect scrollRect;

    [SerializeField]
    GameObject mainObject;

    [SerializeField]
    GameObject gameManagerObject;

    private void Awake()
    {
        int count = stageData.gameObjects.Length;
        for (int i = 0; i < count; i++)
        {
            GameObject gameObject = Instantiate(buttonBase);
            gameObject.name = i.ToString();
            gameObject.transform.SetParent(scrollRect.content.transform);

            Button button = gameObject.GetComponent<Button>();
            int index = i;
            button.GetComponentInChildren<Text>().text = (i + 1).ToString();
            button.onClick.AddListener(()  =>
            {
                
                for (int j = 0; j < mainObject.transform.childCount; j++)
                {
                    DestroyImmediate(mainObject.transform.GetChild(j).gameObject);
                }
                Debug.Log(index);
                /*
                                 GameObject data = stageData.gameObjects[index];

                                data = Instantiate(data);
                                data.transform.SetParent(mainObject.transform, false);
                                 */

                //メインゲーム
                //ゲームマネージャー有効、ゲーム開始
                gameManagerObject.SetActive(true);




                SceneManager.instance.ChangeScene((int)SceneManager.SceneState.Main);

            });
        }
    }








}
