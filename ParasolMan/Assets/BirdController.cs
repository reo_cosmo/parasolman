﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{



    /// <summary>
    /// 遅いスピード　0.04
    /// 早いスピード　0.10
    /// </summary>
    /// 

    [Header("遅いスピード　0.04, 早いスピード　0.10")]
    [SerializeField]
    Vector3 moveVector;

    public Define.Direction dir;

    private void Awake()
    {
        if (dir == Define.Direction.Left)
        {
            var d = this.transform.position;
            d.x = -100;
            transform.LookAt(d);
        }
        else if (dir == Define.Direction.Right)
        {
            var d = this.transform.position;
            d.x = 100;
            transform.LookAt(d);
        }
    }

    void Update()
    {
        if (dir == Define.Direction.Left)
        {
            transform.position -= moveVector;
        }
        else if (dir == Define.Direction.Right)
        {
            transform.position += moveVector;
        }

    }
}
