﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Define;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField]
    Camera mainCamera;

    public GameObject Obj;


    public bool isPlaying = false;

    [SerializeField]
    StageScriptableObject stageData;

    [SerializeField]
    GameObject mainObject;


    private GameObject playerObject;

    private UmbrellaController uController;

    private GameObject fulcrum;

    private GameObject umblleraObject;

    private GameObject clothObject;  //傘の開閉でアクティブ状態を切り替える

    bool opened;

    private Rigidbody umblleraRigid;


    float span;

    float span2;


    //スコア
    float floatScore;
    public int score;
    [SerializeField] 
    private Text scoreText;

    private void Awake()
    {
        instance = this;
    }


    public void InitGame()
    {
        Debug.Log("InitGame");

        playerObject = stageData.gameObjects[0];

        playerObject = Instantiate(playerObject);
        Vector3 makePos = new Vector3(0, -3, 0);
        playerObject.transform.position = makePos;
        playerObject.transform.SetParent(mainObject.transform, false);

        fulcrum = playerObject.transform.Find("fulcrum").gameObject;
        umblleraObject = playerObject.transform.Find("umbllera").gameObject;
        //umblleraRigid = umblleraObject.GetComponent<Rigidbody>();

        uController = umblleraObject.GetComponent<UmbrellaController>();
        uController.gameManager = this;
        uController.mainCamera = mainCamera;
        uController.cameraMoveVector = mainCamera.transform.position;
        uController.state = PlayerState.Fly;

        isPlaying = true;

        //スコア表示リセット
        ScoreUp(0);
    }

    private void Update()
    {
        //右から
        span += Time.deltaTime;
        if(span > 2.0f)
        {
            span = 0;

            Vector3 makePos = mainCamera.transform.position + new Vector3(Random.Range(-7, -3), Random.Range(-2, -5), 10);

            //どの鳥を出すか
            int rnd = Random.Range(0, 2);

            var b = Instantiate(stageData.bird[rnd]);
            b.transform.position = makePos;
            b.transform.SetParent(mainObject.transform, false);

            b.GetComponent<BirdController>().dir = Direction.Right;

            Destroy(b, 10.0f);

        }


        //左から
        span2 += Time.deltaTime;
        if (span2 > 2.0f)
        {
            span2 = 0;

            Vector3 makePos = mainCamera.transform.position + new Vector3(Random.Range(7, 3), Random.Range(-2, -5), 10);

            //どの鳥を出すか
            int rnd = Random.Range(0, 2);

            var b2 = Instantiate(stageData.bird[rnd]);
            b2.transform.position = makePos;
            b2.transform.SetParent(mainObject.transform, false);

            b2.GetComponent<BirdController>().dir = Direction.Left;

            Destroy(b2, 10.0f);

        }
    }


    public void ScoreUp(float s)
    {
        floatScore += s;
        score = Mathf.FloorToInt(floatScore);
        scoreText.text = "Score:\t" + score.ToString();
    }
}
