﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Define;

public class UmbrellaController : MonoBehaviour
{
    public GameManager gameManager;

    public Camera mainCamera;
    Vector3 cameraPos;

    //カメラのy座標とプレイヤーのｙ座標との差
    [Header("プレイヤーが一番上にいるときのカメラの座標")]
    [SerializeField]
    float highCameraPos;

    [Header("プレイヤーが一番下にいるときのカメラの座標")]
    [SerializeField]
    float lowCameraPos;

    //カメラのｙ座標と支点のｙ座標との差
    float dif = 7;

    //カメラ移動量
    public Vector3 cameraMoveVector;

    //回転支点
    [SerializeField] private GameObject fulcrum;

    Vector3 fulcrumPoint;
    Vector3 umbrellaPos;
    
    //真下を向かせるための座標
    Vector3 downPos;

    //真上を向かせるための座標
    Vector3 upPos;

    float deltaTime;
    float delta;


   public Define.PlayerState state;  //プレイヤーの状態

    [SerializeField]
    private GameObject clothObject;

    Vector3 fallVector;

    [Header("通常時の落下スピード")]
    [SerializeField]
    private float openFallSpeed;

    [Header("上昇感（傘を開いて上昇するときのスピード）")]
    [SerializeField]
    private float riseSpeed;

    [Header("下降感（傘を閉じているときの落下スピード）")]
    [SerializeField]
    private float closeFallSpeed;

    [Header("上昇限度ポイント")]
    [SerializeField]
    private float risePoint;

    [Header("下降限度ポイント")]
    [SerializeField]
    private float fallPoint;

    [Header("急降下中スコアアップ量")]
    [SerializeField]
    private float fallingScoreUp;

    [Header("通常時スコアアップ量")]
    [SerializeField]
    private float flyScoreUp;

    [Header("上昇時スコアアップ量")]
    [SerializeField]
    private float risingScoreUp;


    private void OnEnable()
    {
        deltaTime = Time.deltaTime;
        umbrellaPos = this.transform.position;
        fulcrumPoint = fulcrum.transform.position;

        state = PlayerState.Fly;
       

        this.transform.LookAt(fulcrum.transform, -Vector3.forward);

    }

    private void Update()
    {
        if(GameManager.instance.isPlaying == false)
        { 
            return;
        }



        if (Input.GetMouseButtonDown(0))
        {
            //傘閉じる
            state = PlayerState.Fall;

            clothObject.SetActive(false);
            //umblleraRigid.constraints = RigidbodyConstraints.FreezePositionX;


        //カメラと支点とのy軸方向の差を求めておく
        //再び傘を開いたときに、その差まで上昇し、振り子運動を再開する


        }
        else if (Input.GetMouseButtonUp(0))
        {
            //傘開く
            state = PlayerState.Rise;
            clothObject.SetActive(true);
            //umblleraRigid.constraints = RigidbodyConstraints.None;
        }


        if (state == PlayerState.Fly)
        {
            //傘を開いて飛んでいるときの挙動
            delta += deltaTime;

            fallVector.y -= openFallSpeed;

            fulcrumPoint.y -= openFallSpeed;

            umbrellaPos.x = Mathf.Sin(delta) * 2.0f;
            umbrellaPos.y = fallVector.y + Mathf.Abs(Mathf.Sin(delta)) * Mathf.Abs(Mathf.Sin(delta)) / 2;

            this.transform.position = umbrellaPos;
            fulcrum.transform.position = fulcrumPoint;

            this.transform.LookAt(fulcrum.transform, -Vector3.forward);

            //スコアアップ
            gameManager.ScoreUp(flyScoreUp);

        }
        else if(state == PlayerState.Fall)
        {
            //傘を閉じて急降下していくときの挙動

            fallVector.y -= closeFallSpeed;

            fulcrumPoint.y -= closeFallSpeed;

            umbrellaPos.y = fallVector.y;

            this.transform.position = umbrellaPos;
            fulcrum.transform.position = fulcrumPoint;

            downPos = this.transform.position;
            downPos.y = this.transform.position.y - 100;

            this.transform.LookAt(downPos, -Vector3.forward);

            //スコアアップ
            gameManager.ScoreUp(fallingScoreUp);
        }
        else if(state == PlayerState.Rise)
        {
            //傘を開いて上昇するときの挙動

            fallVector.y += riseSpeed;

            fulcrumPoint.y += riseSpeed;

            umbrellaPos.y = fallVector.y;

            this.transform.position = umbrellaPos;
            fulcrum.transform.position = fulcrumPoint;

            upPos = this.transform.position;
            upPos.y = this.transform.position.y + 100;

            //this.transform.LookAt(upPos, -Vector3.forward);
            this.transform.LookAt(fulcrum.transform, -Vector3.forward);

            //スコアアップ
            gameManager.ScoreUp(risingScoreUp);
        }


        //カメラワーク
        CameraWork();
    }


    void CameraWork()
    {
        if(state == PlayerState.Fly)
        {

        }
        else if(state == PlayerState.Fall)
        {
            if (dif > fallPoint)
            {
                dif -= Time.deltaTime*10;
            }
            else
            {
            }
        }
        else if(state == PlayerState.Rise)
        {
            if(dif < risePoint)
            {
                dif += Time.deltaTime*6;
            }
            else
            {
                state = PlayerState.Fly;
            }
        }


        var pos = fulcrum.transform.position;
        pos.z = -10;
        pos.y -= dif;
        mainCamera.transform.position = pos;

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            //ゲームオーバー
            SceneManager.instance.ChangeScene((int)SceneManager.SceneState.Result);

        }
    }
}
